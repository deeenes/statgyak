#
# Author: Leila Gul 2016
#
# Network analysis of the Little Rock Lake food web
#

require(igraph)

g = read.graph('foodweb.csv', format = 'ncol', directed = TRUE)

annot = read.table('little_rock_lake_taxons.csv', sep = '\t', header = TRUE)

V(g)$category = ''
V(g)$label = ''

for (i in 1:vcount(g)){
    if(as.numeric(V(g)$name[i]) > 0){
        V(g)$category[i] = as.character(
            annot$Taxonomic.G.[
                as.numeric(
                    V(g)$name[i]
                )
            ]
        )
    }
}

for (i in 1:vcount(g)){
    if(as.numeric(V(g)$name[i]) > 0){
        V(g)$label[i] = as.character(
            annot$Taxa[
                as.numeric(
                    V(g)$name[i]
                )
            ]
        )
    }
}

V(g)[0]$label <- 'Inorganic carbon'

c_spinglass = spinglass.community(g)

V(g)$spinglass = c_spinglass$membership

methods <- c("fastgreedy", "leading.eigenvector",
             "spinglass", "walktrap", "infomap")
titles <- c("Fastgreedy", "Leading eigenvector",
            "Spinglass", "Walktrap", "Infomap")
refs <- c("Clauset, 2004", "Newman, 2004",
          "Traag, 2009", "Pascal, 2005", "Rosvall 2009")

layout <- layout.fruchterman.reingold(g, repulserad = vcount(g)^2.3,
                                           area = vcount(g)^2.3, maxiter = 1000)

layout <- layout.reingold.tilford(g)

for(i in seq(1:length(methods))){
    cat(paste(c('\t:: Working on', methods[i], '\n'), sep = ''))
    if(is.null(get.vertex.attribute(g, methods[i]))){
        if(methods[i] == 'spinglass'){
            c <- spinglass.community(g, spins = 4)
        }else if(methods[i] != "fastgreedy" & methods[i] != 'leading.eigenvector'){
            c <- do.call(
                paste(methods[i],".community",sep=""),
                c(list(g))
            )
        }else{
            c <- do.call(
                paste(methods[i],".community",sep=""),
                c(list(as.undirected(g)))
            )
        }
    }
    g <- set.vertex.attribute(g, i, value = c$membership)
    V(g)$color <- rainbow(
        length(
            levels(
                as.factor(c$membership)
                )
            )
        )[c$membership]
    for(v in V(g)){
        V(g)$color[v] <- paste(
            strtrim(V(g)$color[v],7),
            "99", sep="")
    }
    cairo_pdf(filename=paste(methods[i],".pdf", sep=""),
                onefile=TRUE, width=48, height=48,
                pointsize=12, family="DINPro")
    par(cex.main = 3, cex.sub = 3)
    plot(g,
        main = "Little Rock Lake foodweb",
        sub = paste(
            "Number of modules: ",
            length(levels(as.factor(c$membership))),
            " :::: ","method: ", titles[i],
            " :: ", refs[i], sep=""),
        layout = layout,
        edge.width = 0.2,
        vertex.label.family = "DINPro",
        vertex.size = 11,
        vertex.label.cex = 2.5,
        vertex.frame.color = "#FFFFFFFF",
        edge.color = "#77777744"
    )
    dev.off()
}

write.graph(g, 'lrl_network.xml', format = 'graphml')
